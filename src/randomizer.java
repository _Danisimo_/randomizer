package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args)
    {
        int result;

        System.out.print("\n Задание №1 \n ");
        Random r = new Random();
        result = r.nextInt();
        System.out.print(result);

        System.out.print("\n Задание №2 \n");
        for(int i = 0 ;i <10; i++ )
        {
            result = r.nextInt();
            System.out.print(result + "\t");
        }

        System.out.print("\n Задание №3 \n");
        for(int i = 0; i <10; i++)
        {
            result = r.nextInt(11);
            System.out.print(result + "\t");
        }

        System.out.print("\n Задание №4 \n");
        for(int i =0;i<10; i++)
        {
            int min=20;
            int max=50;

            result =r.nextInt((max - min) +1 ) + min;
            System.out.print(result + "\t");
        }

        System.out.print("\n Задание №5 \n");
        for(int i = 0; i <10;i++)
        {
            int min = -10;
            int max = 10;

            result= r.nextInt((max-min) + 1) + min;
            System.out.print(result + "\t");
        }

        System.out.print("\n Задание №6 \n");

        int minQuantity = 3;
        int maxQuantity = 15;

        int quantity = r.nextInt((maxQuantity-minQuantity)+1) + minQuantity;

        for(int i = 0; i < quantity; i++)
        {
            int min = -10;
            int max = 35;

            result = r.nextInt((max-min)+1) + min;
            System.out.print(result + "\t");
        }

    }
}
